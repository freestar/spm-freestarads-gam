# spm-freestarads-gam

Swift package manager for FreestarAds SDK Adapter (GAM)

## Getting started

For integration instructions, please navigate to our [public wiki](https://github.com/freestarcapital/SDK_documentation_iOS/wiki).  Thank you!

