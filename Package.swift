// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// 10.9.2
let package = Package(
    name: "FreestarAds-GAM",
     platforms: [
        .iOS(.v11),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FreestarAds-GAM",
            targets: [
                "FreestarAds-GAM",
                "FreestarAds-GAM-Core",
                "FreestarAds-GMA-Dependencies-GAM-Wrapper"
                ]
            )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "https://gitlab.com/freestar/spm-freestarads-gma-dependencies.git",
            exact: "10.9.0"
            ),
        .package(
            url: "https://gitlab.com/freestar/spm-freestarads-core.git",
            from: "5.30.0"
            )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(
            name: "FreestarAds-GAM",
            url: "https://gitlab.com/freestar/spm-freestarads-gam/-/raw/10.9.2/FreestarAds-GAM.xcframework.zip",
            checksum: "3d7f6fe891e4f9b3516580508c87a15f78fce0989436b1b5f728a26ac8569ec9"
            ),
        .target(
            name: "FreestarAds-GAM-Core",
                dependencies: [
                    .product(name: "FreestarAds", package: "spm-freestarads-core")
                ]
            ),
        .target(
            name: "FreestarAds-GMA-Dependencies-GAM-Wrapper",
                dependencies: [
                    .product(name: "FreestarAds-GMA-Dependencies", package: "spm-freestarads-gma-dependencies")
                ]
            )
    ]
)
